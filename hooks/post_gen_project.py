#!/usr/bin/env python
import os

PROJECT_DIRECTORY = os.path.realpath(os.path.curdir)

if __name__ == '__main__':
    os.rename(f"{PROJECT_DIRECTORY}/.gitignore.sample", f"{PROJECT_DIRECTORY}/.gitignore")
    print("Success!")
