# Obsidian Cookiecutter

A template to set up an [Obsidian](https://obsidian.md/) environment.

## Install

Install [cookiecutter](https://cookiecutter.readthedocs.io/en/stable/index.html) and clone the repository

```bash
$ cookiecutter /path/to/obsidian-template
$ cd /path/to/project
```

Open new Obsidian vault using src folder as root. 
