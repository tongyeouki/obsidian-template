# Notes

## Structure

Root level:

- `bin`: binaries and bash scripts to manage repository
- `src`: knowledge database
- chore files, as `.gitignore`, `Makefile`, `.prettierrc` for

In `src`:

- `.obsidian`: obsidian base config folder
- `dev`: main input sources and devices
- `etc`: configuration and templates
- `home`: personal projects
- `media`: images embedded inside the notes
- `notes`: the most important place

## Usage

### Hotkeys

- `ctrl + U` : pin note
- `ctrl + L` : create link
- `ctrl + Shift + I` : insert template

### Update structure

```bash
$ ./bin/update.sh
```

## Plugins

- Advanced Slides
- Advanced Tables
- Calendar
- Dataview
- Day Planner
- Footnote Shortcut
- Jump To Link
- Kanban
- Kindle Highlights
- Mermaid Tools
- Mind Map
- Note Refactor
- Pandoc
- PlantUML
- Tasks
