#!/bin/bash

KEYWORD=$1
REPLACE=$2

find src/notes -name "*.md" -type f -print0 | xargs -0 sed -i '' "s|\[\[$KEYWORD\]\]|\[\[$REPLACE\]\]|"

# find . -name "*.md" -type f -print0 | xargs -0 sed -i 's|*|-|g'
