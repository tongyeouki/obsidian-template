#!/bin/bash

BASE_DIR="/tmp/obsidian-cookiecutter"
BACKUP_DIR="/tmp/obsidian-backup"
rm -rf $BASE_DIR $BACKUP_DIR
git -C /tmp/ clone git@gitlab.com:tongyeouki/obsidian-cookiecutter.git 
mkdir $BACKUP_DIR
cp -rf . $BACKUP_DIR

# cp -rf $BASE_DIR/{{cookiecutter.slug}}/.obsidian .
# cp -rf $BASE_DIR/{{cookiecutter.slug}}/.obsidian.mobile .
# cp -rf $BASE_DIR/{{cookiecutter.slug}}/bin .
# cp -rf $BASE_DIR/{{cookiecutter.slug}}/dev .
cp -rf $BASE_DIR/{{cookiecutter.slug}}/etc .
# cp -rf $BASE_DIR/{{cookiecutter.slug}}/Makefile .
# cp -rf $BASE_DIR/{{cookiecutter.slug}}/.gitignore.sample .gitignore
