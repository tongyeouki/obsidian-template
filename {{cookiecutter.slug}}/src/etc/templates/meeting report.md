---
project:
title: {{title}}
date: {{date}}
tags:
---

## Agenda


## Attendees

- xxx

## TL;DR

---

### Value proposition

### Clients follow-up

### Stakeholders follow-up

### Product feedbacks

### Key resources

### Key activities

### Financials

---

| Quoi | Comment | Qui | Quand | Priorité | Domaine |
| ---- | ------- | --- | ----- | :------: | :-----: |
|      |         |     |       |          |         |

---

**Check-list**

- [ ] Write note
- [ ] Send note to attendees
- [ ]  Shared the note with stakeholders
- [ ]  Met with key stakeholders to check their level of information
- [ ]  Assessed the leadership
- [ ]  Assessed the meeting outcomes
- [ ]  Who are the critical people to meet?
- [ ]  What can I improve?