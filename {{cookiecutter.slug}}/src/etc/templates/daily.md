---
resource: daily
title: {{title}}
date: {{date}}
tags:
  - daily
---
## Day planner

## Events

```tasks
scheduled on {{title}}
```

## Tasks

```tasks
not done
due on or before {{title}}
```

## Log