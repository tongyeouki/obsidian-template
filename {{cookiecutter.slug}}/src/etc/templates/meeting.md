---
resource: index
title: {{title}}
date: {{date}}
tags:
---

MoC:

## Agenda

## Attendees

## Summary

## Situation

## Notes

## Actions

| What | How | Who | When | Priority | Domain |
| ---- | --- | --- | ---- | :------: | :----: |
|      |     |     |      |          |        |

---

**Resources**

**Links**
